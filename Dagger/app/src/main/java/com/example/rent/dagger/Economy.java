package com.example.rent.dagger;

/**
 * Created by RENT on 2017-01-19.
 */

public class Economy {

    private final Army army;
    private final Buildings buildings;
    private final Population population;

    public Economy(Army army, Buildings buildings, Population population) {
        this.army = army;
        this.buildings = buildings;
        this.population = population;
    }

    public int countCost(){
        return army.countCost() + buildings.countCost();
    }
    public int countGain(){
        return population.countTaxes() + buildings.countProduction();
    }
}
