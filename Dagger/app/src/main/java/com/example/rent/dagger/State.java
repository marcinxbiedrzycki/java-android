package com.example.rent.dagger;

import com.example.rent.dagger.Economy;

/**
 * Created by RENT on 2017-01-19.
 */

public class State {
    private int gold = 1000;
    private Economy economy;

    public State(Economy economy){
        this.economy = economy;
    }

    public void countBalance(){
        gold += economy.countGain();
        gold -= economy.countCost();
    }
}
