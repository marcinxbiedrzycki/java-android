package com.example.rent.dagger;

/**
 * Created by RENT on 2017-01-19.
 */

public class Army {
    private final com.example.rent.dagger.Population population;

    @Inject
    public Army(com.example.rent.dagger.Population population){
        this.population = population;
    }

    public int countCost(){
        int costPerSoldier = 100;
        double percentOfActiveSoldiers = 0.1;

        return (int) (costPerSoldier * percentOfActiveSoldiers * population.getCount());
    }
}
