package com.example.rent.dagger;

import com.example.rent.dagger.Army;
import com.example.rent.dagger.Economy;
import com.example.rent.dagger.Population;
import com.example.rent.dagger.State;

import org.junit.Test;

public class IntegrationTest {



    @Test
    public void test() {

        Building building = new Building();
        Population population = new Population();
        Army army = new Army(population);
        Economy economy = new Economy(army, population, building);
        State state = new State(economy);

        population.setCount(1000);
        System.out.println(state.getStanSkarbca());
        state.countBalans();
        System.out.println((state.getStanSkarbca()));

    }
}